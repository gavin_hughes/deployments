module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  semi: false,
  trailingComa: 'none',
  arrowParens: 'always',
};
