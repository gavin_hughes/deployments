describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative()
  })

  it('should render home screen', async () => {
    await expect(element(by.id('home_screen'))).toBeVisible()
  })

  it('should navigate to settings sceen and back', async () => {
    await element(by.id('settings_button')).tap()
    await expect(element(by.id('settings_screen'))).toBeVisible()
    await element(by.id('back_button')).tap()
    await expect(element(by.id('home_screen'))).toBeVisible()
  })
})
