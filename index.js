import {AppRegistry} from 'react-native'
import codePush from 'react-native-code-push'

import App from './App'
import {name as appName} from './app.json'

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.MANUAL
}

const CodePushApp = codePush(codePushOptions)(App)

AppRegistry.registerComponent(appName, () => CodePushApp)
