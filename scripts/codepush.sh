DEPLOYMENT_NAME="dev-$CI_COMMIT_REF_SLUG"

./node_modules/.bin/appcenter login --token $CODEPUSH_API_TOKEN
./node_modules/.bin/appcenter codepush deployment add -a perkbox/Perkbox $DEPLOYMENT_NAME
