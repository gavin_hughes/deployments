import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {IconButton, Button} from 'react-native-paper'

import Home from '../screens/Home'
import Settings from '../screens/Settings'

const Stack = createStackNavigator()

const options = ({navigation, route}) => {
  return {
    headerRight: route.name === 'Home' ? () => headerRight(navigation) : null,
    headerLeft: route.name === 'Settings' ? () => headerLeft(navigation) : null
  }
}

const headerRight = (navigation) => {
  return (
    <IconButton
      size={24}
      icon="settings"
      testID="settings_button"
      onPress={() => navigation.navigate('Settings')}
    />
  )
}

const headerLeft = (navigation) => {
  return (
    <Button
      color="black"
      testID="back_button"
      onPress={() => navigation.goBack()}>
      Back
    </Button>
  )
}

function RootNavigator() {
  return (
    <Stack.Navigator mode="modal">
      <Stack.Screen component={Home} name="Home" options={options} />
      <Stack.Screen component={Settings} name="Settings" options={options} />
    </Stack.Navigator>
  )
}

export default RootNavigator
