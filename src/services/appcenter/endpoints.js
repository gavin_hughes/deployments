const endpoints = {
  deployments: (url, appOwnerName, appName) => {
    console.log(url, appOwnerName, appName)
    return `${url}/apps/${appOwnerName}/${appName}/deployments`
  }
}

export default endpoints
