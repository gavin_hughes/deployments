import API from '../api'
import endpoints from './endpoints'
import {
  APP_NAME,
  APP_OWNER_NAME,
  APPCENTER_API_KEY,
  APP_DEPLOYMENT_NAME,
  APP_SECRET
} from './constants'

class AppCenterService {
  token = APPCENTER_API_KEY
  baseUrl = 'https://api.appcenter.ms/v0.1'

  getDeployments() {
    const url = endpoints.deployments(
      'https://api.appcenter.ms/v0.1',
      APP_OWNER_NAME,
      APP_NAME
    )
    const options = {
      headers: {
        ['X-API-TOKEN']: APPCENTER_API_KEY
      }
    }

    return API.get(url, options)
      .then((releases) => releases)
      .catch((e) => console.log(`Error: ${e}`))
  }
}

const service = new AppCenterService()

export default service
