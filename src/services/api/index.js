class API {
  async get(url, options = {}) {
    try {
      // 🤮
      return await (await fetch(url, options)).json()
    } catch (e) {
      throw e
    }
  }
}

const service = new API()

export default service
