import React from 'react'
import {render, wait} from '@testing-library/react-native'

import Home from '../Home'

test('<Home /> renders', async () => {
  const {getByTestId} = render(<Home />)
  await wait(() => expect(getByTestId('home_screen')).toBeTruthy())
})
