import React from 'react'
import {View, StyleSheet} from 'react-native'
import {Headline} from 'react-native-paper'

function Home() {
  return (
    <View style={styles.container} testID="home_screen">
      <Headline>Home</Headline>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Home
