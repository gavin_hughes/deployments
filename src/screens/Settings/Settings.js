import React, {useEffect, useState} from 'react'
import {View, StyleSheet, FlatList, Button} from 'react-native'
import {Headline} from 'react-native-paper'
import codepush from 'react-native-code-push'

import appCenterService from '../../services/appcenter'

function Home() {
  const [deployments, setDeployments] = useState([])

  useEffect(() => {
    appCenterService.getDeployments().then((deployments) => {
      setDeployments(deployments)
    })
  }, [])

  const handleOnPress = (deploymentKey) => {
    codepush.sync({deploymentKey, installMode: 0})
  }

  const renderItem = ({item}) => {
    return <Button onPress={() => handleOnPress(item.key)} title={item.name} />
  }

  return (
    <View style={styles.container} testID="settings_screen">
      <Headline>SETTINGS</Headline>
      <FlatList
        data={deployments || []}
        renderItem={renderItem}
        keyExtractor={(item) => item.key}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Home
